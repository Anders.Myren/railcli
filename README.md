```
    Usage:
        $0 [-lsh]

    Options
        -h    	       Show this helptext 
        -s             Install and setup tools
        -c             Generate KUBECONF files
        -l \${user}    Login to rail loginhost
        -u \${user}    Sshuttle to rail loginhost
```
